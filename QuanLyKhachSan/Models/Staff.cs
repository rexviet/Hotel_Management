﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Staff
    {
        string _id, _name, _phone;
        Department _department;
        Position _pos;

        public string Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Phone
        {
            get
            {
                return _phone;
            }

            set
            {
                _phone = value;
            }
        }

        public Department Department
        {
            get
            {
                return _department;
            }

            set
            {
                _department = value;
            }
        }

        public Position Pos
        {
            get
            {
                return _pos;
            }

            set
            {
                _pos = value;
            }
        }
    }
}
