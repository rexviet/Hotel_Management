﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DAT
{
    public class Connection
    {
        const string _connectString = "Server=tcp:rexvietserver.database.windows.net,1433;Initial Catalog=QuanLyKhachSan;Persist Security Info=False;User ID=rexviet;Password=Viet123!@#;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        static Connection _instance;
        SqlConnection _conn;
        Connection()
        {
            _conn = new SqlConnection(_connectString);
        }

        public string TryConnect()
        {
            try
            {
                _conn.Open();
                return ("connect successful!");
            }
            catch (Exception ex)
            {
                return (ex.Message);
            }
        }

        public static Connection Instance
        {
            get {
                if (_instance == null)
                    _instance = new Connection();
                return _instance;
            }
        }

        public SqlDataReader ExcuteQuery(string sql)
        {
            try
            {
                _conn.Open();
                SqlCommand cmd = new SqlCommand(sql, _conn);
                SqlDataReader dataReader = cmd.ExecuteReader();
                cmd.Dispose();
                return dataReader;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ExcuteNonQuery(string sql)
        {
            try
            {
                _conn.Open();
                SqlCommand cmd = new SqlCommand(sql, _conn);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                _conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CloseConnection()
        {
            if (_conn.State == System.Data.ConnectionState.Open)
            {
                Console.WriteLine("close connection");
                _conn.Close();
            }
        }
    }
}
